import math
import json
import pprint
from PIL import Image, ImageDraw, ImageFont

def load_map_data(json_file):
    with open(json_file) as json_data:
        map_data = json.load(json_data)
    return map_data

def map_size(map_data):
    line_numbers = map(int, map_data)
    total_lines = max(line_numbers) + 1
    characters_per_line = max(map(lambda line_number: len(map_data[line_number]['text']), map_data)) + 1
    print(f'map_size = {total_lines}, {characters_per_line}')
    return (total_lines, characters_per_line)

def convert_BGR_to_RGB(BGR):
    blue = BGR[0:2]
    green = BGR[2:4]
    red = BGR[4:6]
    return red + green + blue

def create_blank_map(character_width, row_height, total_lines, characters_per_line, border):
    map_width = (characters_per_line * character_width) + (border * 2)
    map_height = (total_lines * row_height) + (border * 2)
    print(f'Map width {map_width} height {map_height}')
    im = Image.new("RGB", (map_width, map_height))
    draw = ImageDraw.Draw(im)    
    return im, draw

def print_line(text, colors, line_number, print_text, start_x):    
    current_color = colors[0:6] #Get the 6 characters of the hex color
    end_of_last_color = 0
    next_x = start_x
    for c in range(0, len(text)):
        character_color = colors[c*6:(c+1)*6]
        if character_color != current_color:            
            next_x = print_text(text[end_of_last_color:c], '#' + convert_BGR_to_RGB(current_color), next_x, line_number)
            end_of_last_color = c
            current_color = character_color    
    #Draw any remaining characters                     
    print_text(text[end_of_last_color:], '#' + convert_BGR_to_RGB(current_color), next_x, line_number)

def print_map(map_data, print_text, total_lines, border):
    for line_number in range(0, total_lines):
        if str(line_number) in map_data:        
            #TODO: Background line, Style line
            text = map_data[str(line_number)]['text']        
            colors = map_data[str(line_number)]['colorLine']       
            print_line(text, colors, line_number, print_text, border)
        if line_number % 100 == 0:
            print(f'Done line {line_number}')

def text_map(map_data, save_as, font, border):
    total_lines, characters_per_line = map_size(map_data)

    line_spacing = 0
    test_string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' #TODO: Use some EM characters to do this instead

    font_width = math.floor(font.getsize(test_string)[0] / len(test_string))
    font_height = font.getsize('ABCDEFGHIJKLMNOPQRSTUVWXYZ')[1] + line_spacing

    #print(f'Character width {characterwidth(font)} height {rowheight(font)}')
    im, draw = create_blank_map(font_width, font_height, total_lines, characters_per_line, border)
   
    def print_text(text, color, x, line_number):
        pixel = (x, (font_height * line_number) + border)
        draw.text(pixel, text, font=font, fill=color)
        return x + font.getsize(text)[0]
    print_map(map_data, print_text, total_lines, border)
    del draw
    im.save(save_as, "PNG")

def color_map(map_data, save_as, character_width, character_height, line_spacing, border):
    total_lines, characters_per_line = map_size(map_data)
    line_height = character_height + line_spacing
    im, draw = create_blank_map(character_width, line_height, total_lines, characters_per_line, border)
   
    def print_colors(text, color, x, line_number):
        length_of_text = (len(text)*character_width)
        starting_height = (line_height * line_number) + border        
        draw.rectangle([x, starting_height, x+length_of_text, starting_height+character_height], fill=color)
        return x + length_of_text
    print_map(map_data, print_colors, total_lines, border)
    del draw
    im.save(save_as, "PNG")    
    
def make_color_maps(map_data, border, sizes):
    for size in sizes:
        width = size
        height = size*2
        #spacing = math.floor(size*2/4)
        spacing = 0
        name = f"map_color_{width}_{height}_{spacing}.png"
        color_map(map_data, name, width, height, spacing, border)
        print(f"Done size {name}")

def make_text_maps(map_data, border, sizes):
    for size in sizes:
        font = ImageFont.truetype("lucon.ttf", size=size)    
        name = f"map_text_{size}.png"
        text_map(map_data, name, font, border)
        print(f"Done size {name}")

map_data = load_map_data("alyriaVmap.json")['lines']
#map_data = load_map_data("fixedMap.json")['lines']
color_map(map_data, "map_color_4_3_1.png", 4, 3, 1, 0)
#make_text_maps(map_data, 10, [15])
#color_map(map_data, "map_color_1_1_0.png", 1, 1, 0, 10)
#make_color_maps(map_data, 10, (1,2,3,4,5,6,7,8,9,10))
make_text_maps(map_data, 10, (3,6,12,15,18,21,24))

'''
def repair_map(json_file):
    with open(json_file) as json_data:
        map_data = json.load(json_data)
    
    for line_number in map_data['lines']:
        map_data['lines'][line_number]['text'] = map_data['lines'][line_number]['text'][0:2300]
        map_data['lines'][line_number]['styleLine'] = map_data['lines'][line_number]['styleLine'][0:2300]
        map_data['lines'][line_number]['colorLine'] = map_data['lines'][line_number]['colorLine'][0:2300*6]
        map_data['lines'][line_number]['backgroundLine'] = map_data['lines'][line_number]['backgroundLine'][0:2300*6]

    with open('fixedMap.json', 'w') as json_file:
        json.dump(map_data, json_file)

repair_map("alyriaVmap.json")
'''